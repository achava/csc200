from gasp import *
from random import randint
from time import sleep

class RobotsGame:
    LEFT_KEY = "A"
    UP_LEFT_KEY = "Q"
    UP_KEY = "W"
    UP_RIGHT_KEY = "E"
    RIGHT_KEY = "D"
    DOWN_RIGHT_KEY = "C"
    DOWN_KEY = "S"
    DOWN_LEFT_KEY = "Z"
    TELEPORT_KEY = "T"
    RIGHT_EDGE = 63
    LEFT_EDGE = 0
    TOP_EDGE = 47
    BOTTOM_EDGE = 0

    def __init__(self):
        begin_graphics()
        self.finished = False
        self.player = Player()
        self.robot = Robot()

    def next_move(self):
        self.player.move()
        self.robot.move(self.player)
        # self.player.check_collisions(self.robot)

    def check_collisions(self):
        if (self.Player.x,self.Player.y) == (self.Robot.x,self.Robot.y):
            print("all done")

    def over():
        end_graphics()


class Player:
    def __init__(self):
        self.place()

    def place(self):
        self.x = randint(RobotsGame.LEFT_EDGE, RobotsGame.RIGHT_EDGE)
        self.y = randint(RobotsGame.BOTTOM_EDGE, RobotsGame.TOP_EDGE)
        self.shape = Circle((10 * self.x, 10 * self.y), 5, filled=True)

    def safe_place(self):
        self.place()
        if (self.x,self.y) == (Robot.self.x,Robot.self.y):
            self.place()

    def teleport(self):
        remove_from_screen(self.shape)
        self.place()

    def move(self):
        key = update_when("key_pressed")

        while key == RobotsGame.TELEPORT_KEY:
            self.teleport()
            key = update_when("key_pressed")

        if key == RobotsGame.RIGHT_KEY and self.x < RobotsGame.RIGHT_EDGE:
            self.x += 1
        elif key == RobotsGame.DOWN_RIGHT_KEY:
            if self.x < RobotsGame.RIGHT_EDGE:
                self.x += 1
            if self.y > RobotsGame.BOTTOM_EDGE:
                self.y -= 1
        elif key == RobotsGame.DOWN_KEY and self.y > RobotsGame.BOTTOM_EDGE:
            self.y -= 1
        elif key == RobotsGame.DOWN_LEFT_KEY:
            if self.x > RobotsGame.LEFT_EDGE:
                self.x -= 1
            if self.y > RobotsGame.BOTTOM_EDGE:
                self.y -= 1
        elif key == RobotsGame.LEFT_KEY and self.x > RobotsGame.LEFT_EDGE:
            self.x -= 1
        elif key == RobotsGame.UP_LEFT_KEY:
            if self.x > RobotsGame.LEFT_EDGE:
                self.x -= 1
            if self.y < RobotsGame.TOP_EDGE:
                self.y += 1
        elif key == RobotsGame.UP_KEY and self.y < RobotsGame.TOP_EDGE:
            self.y += 1
        elif key == RobotsGame.UP_RIGHT_KEY:
            if self.x < RobotsGame.RIGHT_EDGE:
                self.x += 1
            if self.y < RobotsGame.TOP_EDGE:
                self.y += 1

        move_to(self.shape, (10 * self.x, 10 * self.y))


class Robot:
    def __init__(self):
        self.place()

    def place(self):
        self.x = randint(RobotsGame.LEFT_EDGE, RobotsGame.RIGHT_EDGE)
        self.y = randint(RobotsGame.BOTTOM_EDGE, RobotsGame.TOP_EDGE)
        self.shape = Box((10 * self.x, 10 * self.y), 10, 10)

    def move(self, player):
        if self.x < player.x:
            time.sleep(0.1)
            self.x += 1
        elif self.x > player.x:
            time.sleep(0.1)
            self.x -= 1

        if self.y < player.y:
            time.sleep(0.1)
            self.y += 1
        elif self.y > player.y:
            time.sleep(0.1)
            self.y -= 1

        move_to(self.shape, (10 * self.x, 10 * self.y))


game = RobotsGame()

while not game.finished:
    game.next_move()

game.over()

