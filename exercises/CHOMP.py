from gasp import *

GRID_SIZE = 30
MARGIN = GRID_SIZE

BACKGROUND_COLOR = color.BLACK
WALL_COLOR = "#99E5E5"

class Immovable:
    pass

class Nothing(Immovable):
    pass

class Wall(Immovable):
    def __init__(self, maze, point):
        self.place = point
        self.screen_point = maze.to_screen(point)
        self.maze = maze
        self.draw()

    def draw(self):
        (screen_x, screen_y) = self.screen_point
        dot_size = GRID_SIZE * 0.2
        Circle(self.screen_point, dot_size, color=WALL_COLOR, filled=True)

class Maze:
    def __init__(self):
        self.have_window = False
        self.game_over = False
        self.get_level()
        self.height = len(self.the_layout)
        self.width = len(self.the_layout[0])
        self.make_window()

    def get_level(self):
        f = open("layout.dat")
        self.the_layout = []
        for line in f.readlines():
            self.the_layout.append(line.rstrip())

    def finished(self):
        return self.game_over

    def play(self):
        answered = input("are we done yet? ")
        if answered == "y":
            self.game_over = True
        else:
            print("i'm playing")

    def done(self):
        print("I'm done") 

    def make_window(self):
        grid_width = (width - 1) + GRID_SIZE
        grid_height = (height - 1) + GRID_SIZE
        screen_width = 2 * MARGIN + grid_width
        screen_height = 2 * MARGIN + grid_height
        begin_graphics(screen_width, screen_height, "Chomp", BACKGROUND_COLOR)

    def to_screen(self, point):
        (x, y) = point
        x = x * GRID_SIZE + MARGIN
        y = y * GRID_SIZE + MARGIN
        return (x, y)

    def make_object(self, point, character):
        (x, y) = point
        if character == "%:":
            self.map[y][x] = Wall(self, point)

the_maze = Maze()

while not the_maze.finished():
    the_maze.play()

the_maze.done()
