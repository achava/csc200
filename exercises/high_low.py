from random import randint

def game():
    global randnum, guesses, question, p_guess
    randnum = randint(1,1000)
    guesses = 0
    question = str("What is your guess? ")
    p_guess = 0
    print("OK, I've thought of a number between 1 and 1000.")
    while p_guess != randnum:
        p_guess = int(input(question))
        guesses += 1
        if p_guess == randnum:
            print("That was my number. Well done!")
            break
        if p_guess > randnum:
            print("That's too high")
        elif p_guess < randnum:
            print("That's too low")
    print(f"You took {guesses} guesses")

game()

again = input("Would you like another game (y/n)? ")
if again == "n":
    print("OK. Bye!")
else:
    game()
