from gasp import *
from random import randint

begin_graphics()
finished = False

def place_robot():
    global robot_x, robot_y, robot_shape
    robot_x = randint(0, 63)
    robot_y = randint(0, 47)
    robot_shape = Box((10*robot_x+5, 10*robot_y+5), 5, 5)

def place_player():
    global player_x, player_y, player_shape
    player_x = randint(0, 63)
    player_y = randint(1,46)
    player_shape = Circle((10*player_x+5, 10*player_y+5), 5, filled=True)

def move_robot():
    global robot_x, robot_y, robot_shape
    def up():
        global robot_y
        if robot_y < 47:
            robot_y += 1
    def down():
        global robot_y
        if robot_y > 0:
            robot_y -= 1
    def left():
        global robot_x
        if robot_x > 0:
            robot_x -= 1
    def right():
        global robot_x
        if robot_x < 64:
            robot_x += 1

    key = update_when('key_pressed')

    if key == '6':
        right()
    elif key == '2':
        down()
    elif key == '4':
        left()
    elif key == '8':
        up()
    elif key == '3':
        if robot_x < 63:
            robot_x += 1
        if robot_y > 0:
            robot_y -= 1
    elif key == '1':
        if robot_x > 0:
            robot_x -= 1
        if robot_y > 0:
            robot_y -= 1
    elif key == '7':
        if robot_x > 0:
            robot_x -= 1
        if robot_y < 47:
            robot_y += 1
    elif key == '9':
        if robot_x < 63:
            robot_x += 1
        if robot_y < 47:
            robot_y += 1
    move_to(robot_shape, (10*robot_x+5, 10*robot_y+5))
    print(robot_x, robot_y, "(robot)")

def move_player():
    global player_x, player_y, player_shape
    def up():
        global player_y
        if player_y < 47:
            player_y += 1
    def down():
        global player_y
        if player_y > 0:
            player_y -= 1
    def left():
        global player_x
        if player_x > 0:
            player_x -= 1
    def right():
        global player_x
        if player_x < 64:
            player_x += 1
    key = update_when('key_pressed')

    if key == '6':
        right()
    elif key == '2':
        down()
    elif key == '4':
        left()
    elif key == '8':
        up()
    elif key == '3':
        if player_x < 63:
            player_x += 1
        if player_y > 0:
            player_y -= 1
    elif key == '1':
        if player_x > 0:
            player_x -= 1
        if player_y > 0:
            player_y -= 1
    elif key == '7':
        if player_x > 0:
            player_x -= 1
        if player_y < 47:
            player_y += 1
    elif key == '9':
        if player_x < 63:
            player_x += 1
        if player_y < 47:
            player_y += 1    
    move_to(player_shape, (10*player_x+5, 10*player_y+5))
    print(player_x, player_y, "(player)")

place_robot()
place_player()

while not finished:
    move_player()
    move_robot()

end_graphics()

