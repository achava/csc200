import xml.etree.ElementTree as ET
data = ET.parse('info.xml') 
root = data.getroot()

for child in root:
    gsname = child.get('name')
    for galaxy in child.iter('galaxy'):
        gname = galaxy.get('name')
        print(gsname + ",", gname)

