import json
from jinja2 import Environment, FileSystemLoader

print("working...")

with open("albums.json", "r") as d:
    albums = json.load(d)

#fileLoader = FileSystemLoader("./templates")
env = Environment(loader=FileSystemLoader('./templates'))
rendered = env.get_template("gallery.html").render(albums=albums, title="Gallery") 


# Write HTML to file

fileName = "index.html"

with open(r"./test.txt", "w") as f:
    f.write(rendered)
    print(rendered)

with open(f"./site/{fileName}", "w") as f:
    f.write(rendered)