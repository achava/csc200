from bs4 import BeautifulSoup
import json
from jinja2 import Environment, FileSystemLoader

# Open up souce and pipe to soup
with open("sourcecode.html", "r") as f:
    soup = BeautifulSoup(f, features="html.parser")

# Find h2 "Albums"
albumTag = soup.find("h2", string="Albums")
# Find parent section
albumSection = albumTag.find_parent("section")
# Find list items in section
albums = albumSection.findAll("li")
# Iterate and extract data

allAlbumDetails = [] # List of albums

album = albums[0]
for album in albums:
    aTag = album.find("a")
    albumLink = aTag["href"]
    albumName = aTag["alt"]

    albumImageDiv = aTag.find("div", {"class": "grid-item-image"})
    albumImage = albumImageDiv["data-src"]
    
    albumDetails = {
            "name": albumName,
            "link": albumLink,
            "cover": albumImage
    }

    allAlbumDetails.append(albumDetails)

# Write data to file

with open("albums.json", "w") as a:
    json.dump(allAlbumDetails,a)
# File to website
with open("albums.json", "r") as d:
    albums = json.load(d)

fileLoader = FileSystemLoader("templates")
env = Environment(loader=fileLoader)
rendered = env.get_template("gallery.html").render(albums=albums, title="Gallery") 

print(rendered)
# Write HTML to file

fileName = "index.html"

with open(r"./test.txt", "w") as t:
    t.write(rendered)

with open(f"./site/{fileName}", "w") as f:
    f.write(rendered)
    f.close()