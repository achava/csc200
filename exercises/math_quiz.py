from gasp import *
from random import randint

itr = 0
corr = 0
while itr < 10:
    num1 = randint(1,12)
    num2 = randint(1,12)
    ans = num1 * num2
    question = "What's " + str(num1)  + " times " + str(num2) + "? "
    pans = int(input(question))
    if pans == ans:
        print("That's right - well done.")
        corr += 1
    else:
        print("No, I'm afraid the answer is " + str(ans))
    itr += 1

print("I asked you " + str(itr) + " questions. You got " + str(corr) + " of them right.\nWell done!")
