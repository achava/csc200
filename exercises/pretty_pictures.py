from gasp import *

begin_graphics()

finished = False 
itr = 0

def stickman(x,y):
    Circle((x, y), 90)
    Circle((x-30, y-15), 10)
    Circle((x+30, y-15), 10)
    Arc((x, y), 80, 225, 90)

while itr <= 3:
    stickman(300,300)
    itr += 1

finished = True

if finished == True:
    update_when('key_pressed')
    end_graphics()
